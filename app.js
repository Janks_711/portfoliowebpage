var express         = require("express"),
    app             = express();

//Set Route Variables
var indexRoutes = require("./routes/indexRoutes");


app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));

//Get Routes
app.use(indexRoutes);

app.listen(3000, function(){
    console.log("Portfolio Page Running");
});