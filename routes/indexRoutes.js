var express = require("express");
var router = express.Router();

//Index Route
router.get("/", function(req, res){
    res.render("index");
})

module.exports = router;